var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    watch = require('gulp-watch'),
    browserSync = require('browser-sync').create();

gulp.task('sass', function () {
  gulp.src('sass/**/main.scss')
    .pipe(sass({outputStyle: 'uncompressed'}).on('error', sass.logError))
    .pipe(autoprefixer("last 3 versions", "> 1%", "ie 8"))
    .pipe(gulp.dest('./css'));
});

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
  return gulp.src("sass/*.scss")
    .pipe(sass())
    .pipe(gulp.dest("css"))
    .pipe(browserSync.stream());
});


// Static Server + watching scss/html files
gulp.task('serve', ['sass'], function() {

    browserSync.init({
      server: "./"
    });

    gulp.watch("sass/*.scss", ['sass']);
    gulp.watch("*.html").on('change', browserSync.reload);
});

//browser-sync start --server --files "css/*.css, *.html"

gulp.task('default', ['serve']);
